#!/bin/bash

COUNT=$SERVER_COUNT;
if [ -z "$COUNT" ]; then
  echo "env SERVER_COUNT is empty, 1 will be used"
  COUNT=1
fi

create_certificate()
{
  SNAME=$1
  if [ ! -f "/etc/letsencrypt/live/${SNAME}/fullchain.pem" ]; then
    echo "Criando certificados para o site ${SNAME}"
    certbot certonly --standalone --preferred-challenges http -d ${SNAME} -n --email ${CERTIFICATE_EMAIL} --agree-tos
  else
    certbot renew --quiet
    echo "Certificados para o site $SNAME já existem, renovando."
  fi
}

create_config()
{
  export SERVER_NAME="$1"
  export API_URL="$2"
  SNAME=$1

  if [ ! -f "/etc/nginx/conf.d/${SNAME}.conf" ]; then
    echo "Substituindo variáveis para o site ${SNAME}"
    envsubst '${SERVER_NAME}${API_URL}' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/${SNAME}.conf
  else
    echo "Arquivo de configuração para o site $SNAME já existe, não será recriado"
  fi
}

if [ $COUNT -gt 1 ]; then
  for counter in $(seq 1 $COUNT); do
     sn="SERVER_NAME_$counter"
     ur="API_URL_$counter"
     create_certificate "${!sn}"
     create_config "${!sn}" "${!ur}"
  done
else
  create_certificate $SERVER_NAME
  create_config $SERVER_NAME $API_URL
fi

echo "Starting crond..."
crond start

echo "Starting nginx..."
exec nginx -g "daemon off;"
