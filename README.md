# Nginx HTTPS API Proxy

Proxy Nginx com redirecionamento de requisições e aplicação/renovação automática de certificado do Let's Encrypt


Este Imagem cria um proxy com suporte a múltiplos domínios e aplicação automática de certificado, para páginas HTTPS. 

É usado o serviço de Emissão de certificado do Let's Encript. Em Razão disso, a imagem também agenda o processo de renovação automática de certificado.

### Variáveis
- CERTIFICATE_EMAIL: Email registrado no Let's Encrypt para o certificado. Notificações sobre o certificado são enviadas para este Email.
- SERVER_COUNT: Número de APIs gerenciadas pelo Proxy.
- SERVER_NAME_[X]: Domínio do servidor, usado pelo NGix para fazer o Mapeamento de múltiplos domínios para diferentes containers.
- API_URL_[X]: URL da API para onde as requisições devem ser enviadas.

### Cenário
- Digamos que tenhamos uma máquina Docker com os seguintes domínios mapeados para esta máquina: api.aplicacao.com.br e api.mobile.com.br
- O Domínio api.aplicacao.com.br deve redirecionar todas as requisições para um outro container chamado api existente nesta mesma máquina, na porta 8080
- O Domínio api.mobile.com.br deve redirecionar todas as requisições para o endereço https://google.com
- As seguintes variáveis seriam usadas:
    - SERVER_COUNT: 2
    - SERVER_NAME_1: api.aplicacao.com.br
    - API_URL_1: http://api:8080
    - SERVER_NAME_2: api.mobile.com.br
    - API_URL_2: https://google.com

Esta configuração fará com que o Container, ao ser iniciado, gere e aplique os certificados para os 2 sites configurados e redirecione todas as chamadas recebidas para as apis configuradas automaticamente, sem necessidade de aplicação de certificado em cada uma das APIs.

### Reaproveitamento de Certificado
O Let's Encrypt possui alguns limites de número de certificados que pode ser gerados por hora, dia e mês. Então, para evitar que a imagem gere um novo certificado cada vez que é reiniciado, é importante mapear o repositório de certificados (/etc/letsencrypt) para um repositório persistente no Host:


### Subindo a Imagem

    docker volume create certificates
    docker run --name=proxy -p 80:80 -p 443:443 --restart=always -v certificates:/etc/letsencrypt -e CERTIFICATE_EMAIL=your.email@host.com -e SERVER_COUNT=1 -e SERVER_NAME_1=[your domain] -e API_URL_1=[api endpoint] lordcronos/nginx-https-api-proxy:latest