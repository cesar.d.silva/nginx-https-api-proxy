FROM nginx:alpine

COPY files/default.conf.template /etc/nginx/conf.d/default.conf.template
COPY files/entrypoint.sh entrypoint.sh
COPY files/options-ssl-nginx.conf /etc/ssl/letsencrypt/options-ssl-nginx.conf

RUN dos2unix entrypoint.sh
RUN dos2unix /etc/nginx/conf.d/default.conf.template
RUN dos2unix /etc/ssl/letsencrypt/options-ssl-nginx.conf

RUN chmod 755 entrypoint.sh

RUN apk upgrade
RUN apk update
RUN apk add bash
RUN apk add bash-completion
RUN apk add certbot-nginx

#RUN apk add openssl
#RUN openssl dhparam -out /etc/ssl/certs/dhparam.pem 4096

RUN echo "0       5       *       *       *       certbot renew --quiet --nginx" >> /etc/crontabs/root

ENTRYPOINT ["/entrypoint.sh"]
